terraform {
  required_providers {
    azurerm = {
        source = "hashicorp/azurerm"
        version = "=3.48.0"

    }
  }
}
provider "azurerm" {
    features{}
}

resource "azurerm_resource_group" "pjeprirg" {
    name = var.name
    location = var.location
}

resource "azurerm_storage_account" "pjeprisa" {
    name = var.sa-name 
    location = azurerm_resource_group.pjeprirg.location
    resource_group_name = azurerm_resource_group.pjeprirg.name
    account_tier = var.accounttier
    account_replication_type = var.accountrl
   
}

