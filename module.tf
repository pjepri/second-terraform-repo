terraform {
  required_providers {
    azurerm = {
        source = "hashicorp/azurerm"
        version = "=3.48.0"

    }
  }
}
provider "azurerm" {
    features{}
}

module "pjeprimodule" { 
    source = "./storageAccount"
    name = "pjeprirg"
    location = "westeurope"
    accounttier = "Standard"
    accountrl = "LRS"
    sa-name = "sapjepri"

}
resource "azurerm_linux_virtual_machine" "greta-vm" {
  # (resource arguments)
  name = "greta-vm"
  disable_password_authentication = false 
  admin_username ="greta"
  location = "westeurope"
  network_interface_ids = ["/subscriptions/c0177cf4-3641-4a06-bbf6-40d1ac0a60f8/resourceGroups/gretarg/providers/Microsoft.Network/networkInterfaces/greta-nic"]
  size = "Standard_DS1_v2"
  resource_group_name = "gretarg"
  source_image_id = "/subscriptions/c0177cf4-3641-4a06-bbf6-40d1ac0a60f8/resourceGroups/gretarg/providers/Microsoft.Compute/images/greta_packer_image"
           
  os_disk  {
                caching = "ReadWrite"
                
                disk_size_gb = 30
                name = "gretaosdisk11"
               
                storage_account_type = "Standard_LRS"
                write_accelerator_enabled = false
              }
            
    
}